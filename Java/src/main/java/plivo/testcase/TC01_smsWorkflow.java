package plivo.testcase;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import plivo.utilities.*;

public class TC01_smsWorkflow extends SharedDriver {
	static SoftAssert sa = new SoftAssert();

	public static void SoftAssertTrue(boolean a) {
		sa.assertTrue(a);
	}

	static String sheetName = "TC_Messaging";
	static int rowNum = 2;

	@Test(priority = 1)
	public static void createApp() throws InterruptedException {
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickCreateAnAppButton());
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickGetStartedButton());
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickNewPageButton());
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage
				.enterPageName(TestDataUtil.testData.getCellData(sheetName, "PageName", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickCreateButton());
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage
				.selectModule(TestDataUtil.testData.getCellData(sheetName, "Module1", rowNum)));
		Thread.sleep(2000);
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickAddSMS());
		
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterPhoneNumber(TestDataUtil.testData.getCellData(sheetName, "PhoneNo", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterMessage(TestDataUtil.testData.getCellData(sheetName, "Message", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage.connectStartWithSMSModule()); //=================
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickAddEmail());
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterSMTPUrl(TestDataUtil.testData.getCellData(sheetName, "SmtpURL", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterPort(TestDataUtil.testData.getCellData(sheetName, "Port", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterUsername(TestDataUtil.testData.getCellData(sheetName, "Username", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterPassword(TestDataUtil.testData.getCellData(sheetName, "Password", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterFrom(TestDataUtil.testData.getCellData(sheetName, "From", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterTo(TestDataUtil.testData.getCellData(sheetName, "To", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterSubject(TestDataUtil.testData.getCellData(sheetName, "Subject", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterCC(TestDataUtil.testData.getCellData(sheetName, "cc", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage
				.enterEmailMessage(TestDataUtil.testData.getCellData(sheetName, "EmailMsg", rowNum)));
		
		
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage
				.selectModule(TestDataUtil.testData.getCellData(sheetName, "Module2", rowNum)));
		Thread.sleep(2000);
		SoftAssertTrue(SharedDriver.pageContainer.commonElementsPage.clickAddExit());
		SoftAssertTrue(SharedDriver.pageContainer.createAppPage.connectSMSsentWithExit()); //=================
		Thread.sleep(2000);
		sa.assertAll();
	}
}
