package plivo.utilities;

import plivo.utilities.TestDataUtil;

public class EnvironmentUtil {
	
   public static int environmentRowNum;
       
       static { int rowCount = TestDataUtil.testData.getRowCount("LoginAndConfig");
       for (int i = 2; i <= rowCount; i++) { 
              if (TestDataUtil.testData.getCellData("LoginAndConfig", "Environment", i).equalsIgnoreCase(System.getProperty("env"))) { 
                      environmentRowNum = i; 
                      break;
              }
       }
}

}
