package plivo.utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Sync {

	public static void waitForPageLoad(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	public static boolean waitForObject(WebDriver driver, WebElement element) {
		boolean blResult = false;
		try {
			new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
		}
		return blResult;

	}
	
	public static boolean waitForElementDisappearance(WebDriver driver,  By by) {
		boolean blResult = false;
		try {
			 new WebDriverWait(driver, 60 ).until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
		}
		return blResult;
	}

	
	public static boolean waitForElement(WebDriver driver,  By by) {
		boolean blResult = false;
		try {
			 new WebDriverWait(driver, 60 ).until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
		}
		return blResult;
	}

	public static boolean waitForObject(WebDriver driver, String strLogicalName, WebElement element) {
		boolean blResult = false;
		try {
			new WebDriverWait(driver, 60)
					.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
		}
		return blResult;

	}


	public static void waitForSeconds(int sec) {
		try {
			Thread.sleep( sec * 1000);
		} catch (Exception e) {

		}
	}
}
