package plivo.utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import plivo.pagecontainer.PageContainer;


/**
 * The Class SharedDriver.
 */
public class SharedDriver {

	/** The driver. */
	public static WebDriver driver;

	/** The page container. */
	public static PageContainer pageContainer;

	/**
	 * Creates the driver.
	 */
	
	@BeforeSuite
	public static void createDriver() {
		String strBrowser=	TestDataUtil.testData.getCellData("LoginAndConfig", "browserName", 2);
		String URL =TestDataUtil.testData.getCellData("LoginAndConfig", "URL", 2);
	 if (strBrowser.equalsIgnoreCase("CHROME")) {
			System.out.println("Browser Name : " + strBrowser );
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.navigate().to("http://"+URL);
		}

		pageContainer = new PageContainer(driver);
	}
	
	@AfterSuite
	public static void closeBrowser() {
		System.out.println("Close Browsers");
		try {
			SharedDriver.driver.quit();
			System.out.println("INFO - Closed browser(s)");
		} catch (Exception e) {
			System.out.println("INFO - Unable to close browser(s)-Exception occurred:" + e.getMessage());
		}
		
	}
}
