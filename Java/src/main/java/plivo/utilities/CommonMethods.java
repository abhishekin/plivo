package plivo.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CommonMethods {

	public static boolean enterValue(String strLogicalName, WebElement element, String strValue) {
		boolean isValueEntered = false;
		try {
			element.clear();
			element.sendKeys(strValue);
			if (element.getAttribute("value").equals(strValue)) {
				isValueEntered = true;
				System.out.println("PASS-  Entered '" + strValue + "' in " + strLogicalName + " textbox");
			} else {
				isValueEntered = false;
				System.out.println("FAIL-  Unable to enter '" + strValue + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			System.out.println("FAIL-  Unable to enter " + strValue + " value in " + strLogicalName
					+ " textbox. Exception occurred:" + e.getMessage());
		}

		return isValueEntered;
	}

	public static boolean click(String strLogicalName, WebElement element) {
		boolean isButtonClicked = false;
		try {
			element.click();
			isButtonClicked = true;
			System.out.println("PASS - Clicked " + strLogicalName + " button");
		} catch (Exception e) {
			System.out.println(
					"FAIL - Unable to click " + strLogicalName + " button . Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}

	public static boolean selectModule(WebDriver driver, WebElement element, String Module) {
		boolean isModuleClicked = false;
		try {
			element.click();
			isModuleClicked = true;
			System.out.println("PASS - Clicked " + Module + " button");
		} catch (Exception e) {
			System.out.println("FAIL - Unable to click " + Module + " button . Exception occurred:" + e.getMessage());
		}
		return isModuleClicked;
	}

	public static boolean dragAndDrop(WebDriver driver, String strLogicalName, WebElement source,
			WebElement destination) {
		boolean isElementDropped = false;
		try {
			Actions builder = new Actions(driver);
			builder.dragAndDrop(source, destination).perform();
			isElementDropped = true;
		} catch (Exception e) {
			System.out.println(
					"FAIL - Unable to click " + strLogicalName + " button . Exception occurred:" + e.getMessage());
		}

		return isElementDropped;
	}
}