package plivo.pagecontainer;

import org.openqa.selenium.WebDriver;
import plivo.pages.CommonElementsPage;
import plivo.pages.CreateAppPage;

public class PageContainer {

	public WebDriver driver;
	public CreateAppPage createAppPage;
	public CommonElementsPage commonElementsPage;

	public PageContainer(WebDriver driver) {
		this.driver = driver;
		initPages();
	}

	private void initPages() {
		createAppPage = new CreateAppPage(driver);
		commonElementsPage = new CommonElementsPage(driver);

	}

}
