package plivo.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import plivo.utilities.CommonMethods;
import plivo.utilities.Sync;

public class CommonElementsPage {

	WebDriver driver;

	public CommonElementsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//a[@id ='link-create']")
	private WebElement createAppButton;

	@FindBy(xpath = "//*[@id='intro-dialog-cont']/div[2]/button")
	private WebElement getStarted;

	@FindBy(xpath = "//*[@id='add-page']")
	private WebElement newPage;

	@FindBy(xpath = "/html/body/div[20]/div[3]/button[1]")
	private WebElement create;

	@FindBy(xpath = "//*[@id=\"create-dialog\"]/form/p/input")
	private WebElement pageName;

	@FindBy(xpath = "//*[@id=\"create-dialog\"]/form/p/input")
	private WebElement module;

	@FindBy(xpath = "//*[@id=\"accordion\"]/div[4]/ul/li[3]/a/span")
	private WebElement smsModuleGroup;
	
	@FindBy(xpath = "//*[@id=\"accordion\"]/div[1]/ul/li[1]/a/span")
	private WebElement exitModuleGroup;
	
	@FindBy(xpath = "//*[@id=\"accordion\"]/div[4]/ul/li[2]/a/span")
	private WebElement emailModuleGroup;

	public boolean enterPageName(String PageName) {
		Sync.waitForObject(driver, pageName);
		return CommonMethods.enterValue("Page Name", pageName, PageName);
	}

	public boolean clickNewPageButton() {
		Sync.waitForObject(driver, newPage);
		return CommonMethods.click("NewPage", newPage);
	}

	public boolean clickCreateAnAppButton() {
		Sync.waitForObject(driver, createAppButton);
		return CommonMethods.click("Create App", createAppButton);
	}

	public boolean clickGetStartedButton() {
		Sync.waitForObject(driver, getStarted);
		return CommonMethods.click("Get Started", getStarted);
	}

	public boolean clickCreateButton() {
		Sync.waitForObject(driver, create);
		
		return CommonMethods.click("Create", create);
	}

	public boolean clickAddSMS() {
		Sync.waitForObject(driver, smsModuleGroup);
		return CommonMethods.click("SMS Module", smsModuleGroup);
	}
	
	public boolean clickAddEmail() {
		Sync.waitForObject(driver, emailModuleGroup);
		return CommonMethods.click("Email Module", emailModuleGroup);
	}
	
	public boolean clickAddExit() {
		Sync.waitForObject(driver, exitModuleGroup);
		return CommonMethods.click("Exit Module", exitModuleGroup);
	}

	public boolean selectModule(String Module) {
		WebElement module = driver
				.findElement(By.xpath("//span/following-sibling::a[contains(text(), '" + Module + "')]"));
		Sync.waitForObject(driver, module);
		return CommonMethods.selectModule(driver, module, Module);
	}

}
