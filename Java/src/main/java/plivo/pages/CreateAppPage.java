package plivo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import plivo.utilities.CommonMethods;
import plivo.utilities.Sync;

public class CreateAppPage {

	WebDriver driver;

	public CreateAppPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//textarea[@name='phone_constant']")
	private WebElement smsWidgetPhoneNumber;

	@FindBy(xpath = "//div[3]/div/div[2]/div/table/tbody/tr/td[1]/div/textarea")
	private WebElement smsWidgetMessage;

	@FindBy(xpath = "//input[@name='smtp_url']")
	private WebElement smtpURL;

	@FindBy(xpath = "//input[@name='port']")
	private WebElement port;

	@FindBy(xpath = "//input[@name='username']")
	private WebElement username;

	@FindBy(xpath = "//input[@name='password']")
	private WebElement password;

	@FindBy(xpath = "//textarea[@name='from_constant']")
	private WebElement emailFrom;

	@FindBy(xpath = "//textarea[@name='to_constant']")
	private WebElement emailTo;

	@FindBy(xpath = "//textarea[@name='subject_constant']")
	private WebElement emailSubject;

	@FindBy(xpath = "//textarea[@name='cc_constant']")
	private WebElement emailCC;

	@FindBy(xpath = "//div[3]/div/div/table/tbody/tr/td[1]/div/textarea")
	private WebElement emailMessage;

	@FindBy(css = "#module-0 .mod-south div")
	private WebElement start; // Start Module

	@FindBy(css = "#module-1 .mod-north div")
	private WebElement smsInput; // SMS Module

	public boolean connectStartWithSMSModule() {
		Sync.waitForObject(driver, start);
		return CommonMethods.dragAndDrop(driver, "Start with SMS Module", start, smsInput);
	}
	
	@FindBy(css = "#module-1 .panel-bd .panel-nodes-attached .syn-node-attached-e")
	private WebElement smsNotSent; // SMS Not sent
	
	@FindBy(css = "#module-2 .mod-north div")
	private WebElement emailInput; // SMS Module
	
	public boolean connectSMSNotSentWithEmailInput() {
		Sync.waitForObject(driver, smsNotSent);
		return CommonMethods.dragAndDrop(driver, "Start with SMS Module", smsNotSent, emailInput);
	}

	@FindBy(css = "#module-1 .panel-bd .panel-nodes-attached .syn-node-attached-w")
	private WebElement smsSent; // SMS sent

	@FindBy(css = "#module-3 .mod-north div")
	private WebElement exitForSMSSent; // Exit For SMS Sent
	
	public boolean connectSMSsentWithExit() {
		Sync.waitForObject(driver, exitForSMSSent);
		return CommonMethods.dragAndDrop(driver, "Exit For SMS Sent", exitForSMSSent, exitForSMSSent);
	}
	
	@FindBy(css = "#module-2 .panel-bd .panel-nodes-attached .syn-node-attached-w")
	private WebElement emailSent; // SMS sent

	@FindBy(css = "#module-2 .panel-bd .panel-nodes-attached .syn-node-attached-e")
	private WebElement emailNotSent; // SMS sent

	public boolean enterPhoneNumber(String Phone) {
		Sync.waitForObject(driver, smsWidgetPhoneNumber);
		return CommonMethods.enterValue("smsWidgetPhoneNumber", smsWidgetPhoneNumber, Phone);
	}

	public boolean enterMessage(String Message) {
		Sync.waitForObject(driver, smsWidgetMessage);
		return CommonMethods.enterValue("Message", smsWidgetMessage, Message);
	}

	public boolean enterSMTPUrl(String SMTPURL) {
		Sync.waitForObject(driver, smtpURL);
		return CommonMethods.enterValue("SMTPURL", smtpURL, SMTPURL);
	}

	public boolean enterPort(String Port) {
		Sync.waitForObject(driver, port);
		return CommonMethods.enterValue("Port", port, Port);
	}

	public boolean enterUsername(String Username) {
		Sync.waitForObject(driver, username);
		return CommonMethods.enterValue("Username", username, Username);
	}

	public boolean enterPassword(String Password) {
		Sync.waitForObject(driver, password);
		return CommonMethods.enterValue("Password", password, Password);
	}

	public boolean enterFrom(String From) {
		Sync.waitForObject(driver, emailFrom);
		return CommonMethods.enterValue("From", emailFrom, From);
	}

	public boolean enterTo(String To) {
		Sync.waitForObject(driver, emailTo);
		return CommonMethods.enterValue("To", emailTo, To);
	}

	public boolean enterSubject(String Subject) {
		Sync.waitForObject(driver, emailSubject);
		return CommonMethods.enterValue("Subject", emailSubject, Subject);
	}

	public boolean enterCC(String CC) {
		Sync.waitForObject(driver, emailCC);
		return CommonMethods.enterValue("CC", emailCC, CC);
	}

	public boolean enterEmailMessage(String EmailMsg) {
		Sync.waitForObject(driver, emailMessage);
		return CommonMethods.enterValue("Email Message", emailMessage, EmailMsg);
	}
}
