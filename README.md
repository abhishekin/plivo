# PlivoAssignment

About Framework: 
Selenium , and Java based Page object Model Framework
I have used Maven as Build Tool.
Page Container contains all the Page Objects.
plivo.pages contains Pages with elements , and their subsequent actions/methods.
plivo.utilities contains all the utility methods.

How to run:

Simply run the testcase under plivo.testcase folder using TestNG 
You can add more tests in test suite , and run using TestNG suite incase multiple tests.